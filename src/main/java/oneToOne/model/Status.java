package oneToOne.model;

public enum Status {
    COMPLETED,
    PLANNED
}
